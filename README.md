# TP1 #

### INSTRUCTIONS ###

Créer un site web à l'aide d'HTML5 et CSS3. Aucune librairie tierce n'est autorisée (bootstrap,jquery...)et pas de javascript pour l'instant.

Le sujet de ce TP est l'élaboration d'un blog sur un thème qui vous est cher. ce blog contient au moins trois "billets" qui sont cliquables et amènent
vers leur pages respectives qui détaillent un peu plus leur contenu.

Éléments obligatoires: 

*  main, section, article, header, footer,aside,nav
*  un formulaire de contact contenant au moins des champs: 
  * email
  * tel
  * un champ qui permet de renseigner une date pour recontacter la personne
  * un champ qui permet de renseigner le type d'interlocuteur parmi :
    * presse 
    * fan
    * curieux
   
* vérifier l'exactitude des champs grâce aux mécanismes vus pendant le cours
* certains champs doivent être obligatoires pour que vous puissiez recontacter le visiteur
* utiliser conjointement une transformation et une animation
* utiliser succintement les border-radius, les ombres et les gradients
 
Le style est libre. Le bon gout est obligatoire.  

### Instructions GIT ###

Ne pas travailler sur ce repo!! Faire un import du repo et travailler sur sa propre copie.

### CONTACT ###

clement.aceituno@edu.ece.fr
